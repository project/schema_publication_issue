# Schema.org - PublicationIssue

This module provides the [Schema.org/PublicationIssue](https://schema.org/PublicationIssue) integration in Drupal [Schema.org Metatag](https://www.drupal.org/project/schema_metatag)
module.

## Requirements

This module requires the following modules:

- [Schema.org Metatag](https://www.drupal.org/project/schema_metatag)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the `schema_publication_issue` module on Administration > Extend page.
1. Go to Administration > Configuration > Search and metadata > Metatag > tab
Settings(`/admin/config/search/metatag/settings`), select the required Content
Type (CT) and check the "Schema.org: PublicationIssue" checkbox (screenshot).
1. Select your entity type and bundle, expand the "Schema.org: PublicationIssue"
fieldset and fill in the field mappings you want to publish.

## Maintainers

- Ilcho Vuchkov - [vuil](https://www.drupal.org/u/vuil)

## Supporting organizations

- [MODERNA BG LTD](https://www.drupal.org/moderna-bg-ltd)
