<?php

declare(strict_types = 1);

namespace Drupal\schema_publication_issue\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'PublicationIssue' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_publication_issue",
 *   label = @Translation("Schema.org: PublicationIssue"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/PublicationIssue",
 *   }),
 *   weight = 10,
 * )
 */
class SchemaPublicationIssue extends SchemaGroupBase {

}
