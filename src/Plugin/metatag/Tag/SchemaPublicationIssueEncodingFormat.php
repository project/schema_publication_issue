<?php

declare(strict_types = 1);

namespace Drupal\schema_publication_issue\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Plugin for 'schema_publication_issue_encoding_format' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_publication_issue_encoding_format",
 *   label = @Translation("encodingFormat"),
 *   description = @Translation("Media type typically expressed using a MIME format."),
 *   name = "encodingFormat",
 *   group = "schema_publication_issue",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "url",
 *   tree_parent = {
 *     "Url",
 *     "Text",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaPublicationIssueEncodingFormat extends SchemaNameBase {

}
