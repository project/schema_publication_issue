<?php

declare(strict_types = 1);

namespace Drupal\schema_publication_issue\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_publication_issue_license' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_publication_issue_license",
 *   label = @Translation("license"),
 *   description = @Translation("A license document that applies to this content, typically indicated by URL."),
 *   name = "license",
 *   group = "schema_publication_issue",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "url",
 *   tree_parent = {
 *     "Url",
 *     "CreativeWork",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaPublicationIssueLicense extends SchemaNameBase {

}
