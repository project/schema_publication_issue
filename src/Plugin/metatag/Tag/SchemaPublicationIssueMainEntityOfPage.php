<?php

declare(strict_types = 1);

namespace Drupal\schema_publication_issue\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Plugin for 'schema_publication_issue_main_entity_of_page' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_publication_issue_main_entity_of_page",
 *   label = @Translation("mainEntityOfPage"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. The canonical URL of the publication issue page. Specify mainEntityOfPage when the publication issue is the primary topic of the publication issue page."),
 *   name = "mainEntityOfPage",
 *   group = "schema_publication_issue",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "url",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaPublicationIssueMainEntityOfPage extends SchemaNameBase {

}
