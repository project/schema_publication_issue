<?php

declare(strict_types = 1);

namespace Drupal\schema_publication_issue\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'publisher' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_publication_issue_publisher",
 *   label = @Translation("publisher"),
 *   description = @Translation("REQUIRED BY GOOGLE. Publisher of the publication issue."),
 *   name = "publisher",
 *   group = "schema_publication_issue",
 *   weight = 6,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "organization",
 *   tree_parent = {
 *     "Person",
 *     "Organization",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaPublicationIssuePublisher extends SchemaNameBase {

}
