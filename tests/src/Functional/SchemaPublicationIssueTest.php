<?php

declare(strict_types = 1);

namespace Drupal\Tests\schema_publication_issue\Functional;

use Drupal\Tests\schema_metatag\Functional\SchemaMetatagTagsTestBase;

/**
 * Tests that each of the Schema Metatag Publication Issue tags work correctly.
 *
 * @group schema_metatag
 * @group schema_publication_issue
 */
class SchemaPublicationIssueTest extends SchemaMetatagTagsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['schema_publication_issue'];

  /**
   * {@inheritdoc}
   */
  public $moduleName = 'schema_publication_issue';

  /**
   * {@inheritdoc}
   */
  public $groupName = 'schema_publication_issue';

}
